const gulp = require('gulp'),
	jshint = require('gulp-jshint'),
	jshintReporter = require('jshint-stylish'),
	sass = require('gulp-sass'),
	watch = require('gulp-watch'),
	sassGlob = require('gulp-sass-glob'),
	browserify = require('gulp-browserify'),
	plumber = require('gulp-plumber'),
	babel = require('gulp-babel'),
	uglify = require('gulp-uglify'),
	sassLint = require('gulp-sass-lint'),
	autoprefixer = require('gulp-autoprefixer'),
	webserver = require('gulp-webserver'),
	nunjucks = require('gulp-nunjucks-render');


// Create variables for our project paths so we can change in one place
const paths = {
		style:{
			all: '**/*.scss',
			input: 'main.scss',
			output: 'main.css',
			baseDir: './public/assets/css/'
		}
};

gulp.task('serve', function() {
  gulp.src('public')
    .pipe(webserver({
      livereload: true,
      open: true,
      port: 8888
    }));
});

// Browserify
gulp.task('browserify', function() {
	// Single entry point to browserify
	gulp.src('./script/app.js')
		.pipe(plumber())
		.pipe(browserify({
		  insertGlobals : true
		}))
		.pipe(gulp.dest('./public/assets/js/bundle'))
});

gulp.task('templates', function () {
    var nunjucksConf = {
        path: 'templates',
        envOptions: {
            tags: {
                blockStart: '{%',
                blockEnd: '%}',
                variableStart: '{$',
                variableEnd: '$}',
                commentStart: '{#',
                commentEnd: '#}'
            }
        }
    }

    gulp.src(['templates/*.html', '!templates/index.html'])
        .pipe(nunjucks(nunjucksConf))
        .pipe(gulp.dest('public/'));

    gulp.src('templates/index.html')
        .pipe(nunjucks(nunjucksConf))
        .pipe(gulp.dest('public/', {overwrite: true}));
});

// ------------------------------------------
// Gulp Babelify
// ------------------------------------------
gulp.task('uglify', () => { //Uglifies and transpiles
	return gulp.src('public/assets/js/bundle/app.js')
		.pipe(babel({
			presets: ['es2015']
		}))
		.pipe(uglify())
		.pipe(gulp.dest('./public/assets/js/bundle'));
});

// ------------------------------------------
// gulp sass
// ------------------------------------------
gulp.task('sass', function() {
	return gulp.src('./sass/**/*.scss')
		.pipe(sassGlob())
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(gulp.dest('./public/assets/css/'))
});

// ------------------------------------------
// gulp js lint
// ------------------------------------------
gulp.task('jslint', function(){
	gulp.src(paths.src)
		.pipe(jshint())
		.pipe(jshint.reporter(jshintReporter));
});

// ------------------------------------------
// gulp SASS lint
// ------------------------------------------
gulp.task('sasslint', function () {
  gulp.src('./sass/**/*.scss')
    .pipe(sassLint())
    .pipe(sassLint.format())
    .pipe(sassLint.failOnErrolkr())
});

// Gulp watcher for lint
gulp.task('watchLint', function () {
	gulp.src(paths.src)
		.pipe(watch())
		.pipe(jshint())
		.pipe(jshint.reporter(jshintReporter));
});

// ------------------------------------------
// Bundled tasks - watching / testing etc
// ------------------------------------------

// The watcher see's all + watching scss/html files
gulp.task('watchit', ['serve', 'templates', 'sass', 'browserify'], function() {
    gulp.watch("./sass/**/*.scss", ['sass']);
    gulp.watch("./script/**/*.js", ['browserify']);
    gulp.watch("./templates/**/*.html", ['templates']);
});

gulp.task('build', ['uglify']);

// Prep of assets - ugilify, minify, Babel Transpile
// Linting of Js and Sass
gulp.task('default', ['watchit']);





