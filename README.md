Sainsburys Coding Test
================================

To run locally:

- Make sure you have node and the Gulp CLI installed
- Pull down a copy of this repo
- Install dependencies through 'npm install'
- Run 'gulp' and which will run a local server on port 8888 (Can be changed in the Gulp file to a different port if you so wish)

A working link of both tests can be found here [link](http://nicely-done.me/work/sainsburys "Ian Moss coding test").

Both of these tests have been tested successfully on the following browsers and operating systems:
- Chrome & Firefox - Linux, Window, Mac
- Safari - Mac
- Internet Explorer Edge - Windows
- Chrome Mobile - Android (Motorola Moto G)

Javascript Test
=====================
This test is done using pure Vanilla JavaScript. Other frameworks that could have been used for this include AngularJs and ReactJs. Vanilla was chosen due to my lack of experience with React and a pre requisite of not to use Angular.

Issues I've faced with the test include:
- Ideally, to prevent the page hitting the API too often, it would be best to cache the response every ten seconds
- Because the API doesn't return an image ID by itself, I've used the image link as a unique identifier. If i was to start again, I would store these values in an array and not an object as the author ID is unnecessary
- For scalability, a library such as Angular or React would be best used and the API would be requested through an authorised Ajax request

For deployment, run 'gulp build' to Babelify ES6 scripts to supported JavaScript so it will work in Firefox and non ES6 supported browswers.

The script file for this is under 'main.js' in the '/script/modules/' folder

SASS Test
==============
The SASS challenge was quite straight forward with only a few issues. The only part of the issue that I couldn't perfectly recreate was having the two images align perfectly between the left and right hand side due to the variable lengths of the titles above them.
Depending on the size and complexity of a site like this, I would use a minimal CSS framework for this such as Skeleton or Inuit.css.
All SASS files can be found in the SASS directory in the root of this project.