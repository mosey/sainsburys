'use strict';

// Init object
// Get list of images and append the HTML to the page
(function () {
        window.cb = function(data) {
        // use returned data
       
        const likedPhotos = getStorageItem();

        const items = data.items;
        let imageList = '';
        
        // Get the images and append them to the page 
        for(let item in items) {
            let activeClass = "";

            // Compare the images to object and see what's already selected
            if(likedPhotos !== null && Object.getOwnPropertyNames(likedPhotos).length > 0) {

                for(let photo in likedPhotos) {
                    if(items[item].media.m === photo) {
                        activeClass = 'selected';   
                    }
                }
            }
            
            imageList += `<li 
                class='flickr-image ${activeClass}' 
                data-author='${items[item].author_id}'
                data-imageid='${items[item].media.m}'>
                <img src='${items[item].media.m}'
                    alt='${items[item].title}'
                    description='${items[item].author}'/></li>`;
        }

        const images = document.getElementById('images');
        document.getElementById('images').insertAdjacentHTML('beforeend', imageList);
        getImageInfo();
    }

    const tags = "London";
    const script = document.createElement('script');
            script.src = "http://api.flickr.com/services/feeds/photos_public.gne?format=json&jsoncallback=cb&tags=" + tags;
    document.head.appendChild(script);
})();

// Helper function to retrive local storage item if exists or else return false if not
function getStorageItem() {
    try{  
        return JSON.parse(localStorage.getItem('likedPhotos'));
    }
    catch(e) {
        return false;
    }
}

// Click event handler for each element
function getImageInfo() {
    let listImages = document.querySelector('body').getElementsByClassName('flickr-image');
    for(let i = 0; i < listImages.length; i++) {
        listImages[i].onclick = function(el) {
            let author = this.getAttribute('data-author');
            let imageId = this.getAttribute('data-imageid');
            if(setLocalStorageItem(author, imageId)) {
                this.classList.add('selected')
            } else {
                this.classList.remove('selected')
            }
        }
    }
}

// Set and update local storage object from click event
function setLocalStorageItem(author, image) {
    if(!getStorageItem()) {
        let likedPhotos = {};
        localStorage.setItem('likedPhotos', JSON.stringify(likedPhotos));
    }

    let existingStorage = JSON.parse(localStorage.getItem('likedPhotos')); 
    
    if(Object.getOwnPropertyNames(existingStorage).length === 0) {
        existingStorage[image] = author;
        localStorage.setItem('likedPhotos', JSON.stringify(existingStorage));
        return true; 
    } else {
        if(existingStorage.hasOwnProperty(image)) {
            delete existingStorage[image];
            localStorage.setItem('likedPhotos', JSON.stringify(existingStorage));
            return false; 
        } else {
            existingStorage[image] = author;
            localStorage.setItem('likedPhotos', JSON.stringify(existingStorage));
            return true; 
        }
    }
}